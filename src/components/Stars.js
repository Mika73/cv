import { AiFillStar } from "react-icons/ai";
import { AiOutlineStar } from "react-icons/ai";
import { IconContext } from "react-icons";

const Stars =(props) => {
	let fillStars = [];
	let outlineStars = [];

	for (let i = 0; i < props.number; i++) {
    fillStars.push("star");
  };
	for (let i = 0; i < 5 - props.number; i++) {
    outlineStars.push("star");
  };

	return (
		<div>
			{fillStars.map((fillStar,index) => {
				return(
					<IconContext.Provider key={index} value={{ color: "CadetBlue", size: "1.7em", className: "global-class-name" }}>
						<span>
							<AiFillStar />
						</span>
					</IconContext.Provider>
				)})}
			{outlineStars.map((outlineStar,index) => {
				return(
					<IconContext.Provider key={index} value={{ color: "CadetBlue", size: "1.7em", className: "global-class-name" }}>
						<span>
							<AiOutlineStar />
						</span>
					</IconContext.Provider>
				)})}

		</div>
	);
};
export default Stars
