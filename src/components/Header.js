import HeaderButton from './HeaderButton';

const Header = (props) =>{
	return (
		<div>
			<div>
			<header className="header mt-5">
				<HeaderButton color="info" text="À propos de moi" class="mx-2" link="/home"/>
				<HeaderButton color="secondary" text="CV" class="mx-2 px-4"link="/cv"/>
				<HeaderButton color="info" text="Projets effectués" class="mx-2" link="/projects"/>
				<HeaderButton color="secondary" text="Compétences" class="mx-2"link="/skills"/>
				<HeaderButton color="info" text="contact" class="mx-2"link="contact"/>
			</header>
			<hr />
			</div>
		</div>
	)
}
export default Header
