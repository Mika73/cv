import data from "../data/data.json";

const Home =() => {
	return (
		<div className="my-5 mx-5 introduction">
			<h2 className="mb-5 border-bottom border-info" >
				 {data.introTitle}
			</h2>
			{data.introduction.map((row,index)=>{
				return(
				<p key={index}>{row}</p>
				)
			})}
			<div className="notice border border-secondary rounded p-2 mt-4">
				{data.notice.map((row,index)=>{
					return(
					<p key={index}>{row}</p>

					)
				})}
			</div>
		</div>
	);
};
export default Home
