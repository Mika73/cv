import data from "../data/data.json";
import { FaRegAddressBook } from "react-icons/fa";
import {
  AiOutlineMail,
  AiOutlineMobile,
  AiOutlineGlobal,
  AiFillGitlab,
} from "react-icons/ai";
import { IconContext } from "react-icons";
import Button from "react-bootstrap/Button";
import photo from "../data/myphoto.jpg";
import MyCv from "../data/cv.pdf";

const Sidebar = () => {
  const filterdPhoneNum = data.phoneNumbers.find((x) => x.type === "mobile");

  return (
    <div className="container">
      <div className="flex-container">
        <div className="myPhoto">
          <img src={photo} alt="Mika"></img>
        </div>

        <div className="myInfo">
          <h5>{data.job}</h5>
          <h3 className="mb-5">
            {data.firstName}
            <span> </span>
            {data.lastName}
          </h3>

          <p>
            <IconContext.Provider value={{ size: "1.5em" }}>
              <span>
                <FaRegAddressBook />
              </span>
            </IconContext.Provider>
            <span> </span>
            {data.address.city}
          </p>
          <p>
            <IconContext.Provider value={{ size: "1.5em" }}>
              <span>
                <AiOutlineMobile />
              </span>
            </IconContext.Provider>
            <span> </span>
            {filterdPhoneNum.number}
          </p>
          <p>
            <IconContext.Provider value={{ size: "1.5em" }}>
              <span>
                <AiOutlineMail />
              </span>
            </IconContext.Provider>
            <span> </span>
            <a href={`mailto:${data.mail}`}>{data.mail}</a>
          </p>

          {data.webSite ? (
            <p>
              <IconContext.Provider value={{ size: "1.5em" }}>
                <span>
                  <AiOutlineGlobal />
                </span>
              </IconContext.Provider>
              <span> </span>
              {data.webSite}
            </p>
          ) : (
            ""
          )}
          <p>
            <IconContext.Provider value={{ size: "1.5em" }}>
              <span>
                <AiFillGitlab />
              </span>
            </IconContext.Provider>
            <span> </span>
            <a target="_blank" rel="noreferrer" href={data.git}>
              {data.git}
            </a>
          </p>
        </div>
        <div className="download">
          <Button variant="secondary" className="py-2 px-3 mb-3">
            <a href={MyCv} download={data.downloadCVFileName}>
              {data.downloadCVTitle}
            </a>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
