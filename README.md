# Cv

This react application creates your original web resumes.<br>
Simply enter your personal information file into the json file and upload your resume and photo to the data folder.<br>You can easily update your resume by updating the json file.

## Install

1. Install the dependencies in the local node_modules folder.

```
npm install
```

2. Start project

```
npm start
```

## Personal data

Create a data folder under src and create a data.json file in it.<br> The format is as follows.<br>
You can also upload your resume and photo in the same folder.<br>
<img src="src/img/folders.png" width="150" style="margin:10px" >

```
data.json
{
  "introTitle":"Title of your introduction",
  "introduction": [
    "Your introduction","Your intro second paragraph..."
  ],
  "notice": [
    "Some notices"
  ],
  "firstName": "Your first name",
  "lastName": "Your last name",
  "job": "Your job",
  "photo": "myphoto.jpg(Name of your photo uploaded to the data folder)",
  "address": {
    "streetAddress": "Your street address",
    "city": "Your city",
    "postalCode": "Your postal code"
  },
  "phoneNumbers": [
    {
      "type": "home",
      "number": "0xx-xxx-xxxx"
    },
    {
      "type": "mobile",
      "number": "0xx-xxx-xxxx"
    }
  ],
  "mail": "your-mail@xxx.xxx",
  "webSite": "your website if exist",
  "git": "your git repository",
  "cvTitle": "Title of your cv",
  "downloadCVFileName":"CV.pdf",
  "downloadCVTitle":"Download CV",
  "studies": {
    "title": "Title of your study",
    "studies": [
      {
        "startDate": "YYYY-MM-DD",
        "endDate": "YYYY-MM-DD",
        "name": "Name of your study",
        "descriptions": [
          "Your study description",
          "Second paragraph of your study description..."
        ]
      }
    ]
  },
  "jobs": {
    "title": "Title of your jpb experience",
    "jobs": [
      {
        "startDate": "YYYY-MM-DD",
        "endDate": "(If you are still working, enter a space)",
        "name": "Your company, your post...",
        "descriptions": [
          "Your job description1",
          "Your job description2"
        ]
      },
      {
        "startDate": "YYYY-MM-DD",
        "endDate": "YYYY-MM-DD",
        "name": "Your company, your post...",
        "descriptions": []
      }
    ]
  },
  "project":{
    "title":"Projets effectués",
    "completedProjects":[
      {
        "startDate": "YYYY-MM-DD",
        "endDate": "YYYY-MM-DD",
        "name": "Your project name",
        "descriptions": [
          "Your project description1",
          "Your project description2..."
        ]
      }
    ]
  } ,
  "skillsTitle": "The title of your skills",
  "skillLastUpdate": "YYYY-MM-DD",
  "skills": [
    {
      "skill": "HTML(Self evaluation from 1 to 7)",
      "level": 6
    },
    {
      "skill": "CSS",
      "level": 4
    }
  ],
 "languages": [
    {
      "language": "English(Self evaluation from 1 to 7)",
      "level": 7
    },
    {
      "language": "Spanish",
      "level": 5
    }
  ],
  "Interests": [
    "Your hobby1",
    "Your hobby2..."
  ]
}

```
