import React from "react";
import emailjs from "@emailjs/browser";

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      message: "",
      nameErr: "",
      mailErr: "",
      msgErr: "",
      processingResult: "",
    };
  }
  ValidateEmail = (mail) => {
    const mailformat =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (mail.match(mailformat)) {
      return true;
    } else {
      return false;
    }
  };

  handleSubmit(event) {
    event.preventDefault();
    let errFlag = false;
    if (this.state.name === "") {
      this.setState({ nameErr: "Entrez votre nom" });
      errFlag = true;
    } else {
      this.setState({ nameErr: "" });
    }
    if (this.state.email === "") {
      this.setState({ mailErr: "Entrez votre adress mail" });
      errFlag = true;
    } else if (!this.ValidateEmail(this.state.email)) {
      this.setState({
        mailErr: "Veuillez saisir correctement votre adresse e-mail",
      });
      errFlag = true;
    } else {
      this.setState({ mailErr: "" });
    }
    if (this.state.message === "") {
      this.setState({ msgErr: "Veuillez remplir votre message" });
      errFlag = true;
    } else {
      this.setState({ msgErr: "" });
    }

    if (errFlag === false) {
      emailjs
        .sendForm(
          "service_klerjcc",
          "template_7g0eocc",
          event.target,
          "user_GUioeA73LBW2pGWoR0n3r"
        )
        .then(
          (result) => {
            this.setState({
              processingResult: "Votre message a été bien envoyé !",
            });
          },
          (error) => {
            this.setState({
              processingResult:
                "Aucun message n'a été envoyé. Veuillez réessayer plus tard",
            });
          }
        );
    }
  }

  render() {
    return (
      <>
        <form className="container" onSubmit={(e) => this.handleSubmit(e)}>
          <h2 className="my-5 border-bottom border-info mt-5">Contact</h2>

          <p className="mt-5 contact">
            Merci pour votre visite! <br />
            Si vous avez des questions sur ce site Web, sur moi ou simplement
            dire bonjour, veuillez laisser un message. Je vous répondrai dans
            les plus brefs délais.
          </p>

          <p className="text-success contact">
            {this.state.processingResult}
          </p>

          <input type="hidden" name="contact_number" />
          <div className="row mt-5">
            <div className="col-md-6 form-group">
              <label>Votre Nom</label>
              <input
                type="text"
                name="name"
                className="form-control"
                value={this.state.name}
                onChange={(e) => this.setState({ name: e.target.value })}
              />
              <p className="text-danger">{this.state.nameErr}</p>
            </div>
            <div className="col-md-6 form-group">
              <label>Votre Email</label>
              <input
                type="text"
                name="email"
                className="form-control"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
              />
              <p className="text-danger">{this.state.mailErr}</p>
            </div>
          </div>
          <div className="form-group">
            <label>Votre message</label>
            <textarea
              type="text"
              name="message"
              className="form-control"
              onChange={(e) => this.setState({ message: e.target.value })}
              value={this.state.message}
            />
            <p className="text-danger">{this.state.msgErr}</p>
          </div>

          <input
            type="submit"
            className="btn btn-secondary mb-5"
            value="Envoyer"
          />
        </form>
      </>
    );
  }
}
export default Contact;
