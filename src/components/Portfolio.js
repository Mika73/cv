import data from "../data/data.json";
import { Container } from "react-bootstrap";
import Table from "react-bootstrap/Table";

const Portfolio = (props) => {
  let array = [];
  array = data.project.completedProjects;

  return (
    <div>
      {array.map((row, index) => {
        let sYear = row.startDate.slice(0, 4);
        let sMonth = row.startDate.slice(5, 7);
        let eYear = row.endDate.slice(0, 4);
        let eMonth = row.endDate.slice(5, 7);
        let date = "";
        if(sYear === eYear && sMonth === eMonth){
          date = sMonth + "/" + sYear;
        }else{
          date = sMonth + "/" + sYear + " - " + eMonth + "/" +eYear;
        }

        return (
          <div key={index} className="display">
            <Container fluid>
              <h4 className="p-title mb-3">{row.name}</h4>
              <Table bordered className="portfolio mb-5">
                <tr>
                  <td className="table-title">Nom de l’entreprise, organisme ou association</td>
                  <td><p>{row.companyName}</p></td>
                </tr>
                <tr>
                  <td className="table-title">Période</td>
                  <td><p>{date}</p></td>
                </tr>
                <tr>
                  <td className="table-title">Détail</td>
                  <td>
                    {row.descriptions.map((description, index) => {
                      return (
                        <p  key={index}>
                          - {description}
                        </p>
                      );
                    })}
                  </td>
                </tr>
                <tr>
                  <td className="table-title">URL du site ou GIT</td>
                  <td><p><a target="_blank" rel="noreferrer" href={row.url}>{row.url}</a></p></td>
                </tr>
                <tr>
                  <td className="table-title">Technologies utilisées</td>
                  <td>   {row.technologies.map((technology, index) => {
                      return (
                        <p  key={index}>
                          - {technology}
                        </p>
                      );
                    })}</td>
                </tr>

              </Table>
            </Container>
          </div>
        );
      })}
    </div>
  );
};
export default Portfolio;
