import {React, useState} from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { HashRouter, Route, Redirect, Link } from "react-router-dom";

import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import Home from "./components/Home";
import Cv from "./components/Cv";
import Projects from "./components/Projects";
import Skills from "./components/Skills.js";
import Contact from "./components/Contact";
import ScrollButton from "./components/ScrollButton";

import "./index.css";

const App = () => {
  const [hamburger, setHamburger] = useState(true);
  const [menu, setMenu] = useState(false);

  return (
    <Container fluid>
      <Row>
        <Col sm={0} lg={1} className="side"></Col>
        <Col sm={12} lg={3} className="sidebar">
          <div
            onClick={() => {
              setHamburger(!hamburger);
              setMenu(!menu);
            }}
            className={`${hamburger ? "hamburger" : "hide-hamburger"}`}
          >
            <div className="hamburger_bar"></div>
            <div className="hamburger_bar"></div>
            <div className="hamburger_bar"></div>
          </div>

          <div
            onClick={() => {
              setMenu(!menu);
              setHamburger(!hamburger);
            }}
            className={`${menu ? "menu" : "hide-menu"}`}
          >
            <div className="cross">
              <div className="cross_bar1"></div>
              <div className="cross_bar2"></div>
            </div>
            <HashRouter>
              <div className={`${menu ? "menu" : "hide-menu"}`}>
                <div className="mobile-menu">
                  <Link to="/home">
                    <Button variant="info" className="mx-2">
                      À propos de moi
                    </Button>
                  </Link>
                  <Link to="/cv">
                    <Button variant="secondary" className="mx-2">
                      CV
                    </Button>
                  </Link>
									<Link to="/projects">
                    <Button variant="info" className="mx-2">
										Projets effectués
                    </Button>
                  </Link>
									<Link to="/skills">
                    <Button variant="secondary" className="mx-2">
										Compétences
                    </Button>
                  </Link>
									<Link to="/contact">
                    <Button variant="info" className="mx-2">
										Contact
                    </Button>
                  </Link>
                </div>
              </div>
            </HashRouter>
          </div>

          <Sidebar />
        </Col>
        <Col sm={12} lg={7} className="main">
          <HashRouter>
            <Header />
            <div>
              <Route path="/" />
              <Redirect to="/home" />
            </div>
            <Route path="/home" component={Home} />
            <Route path="/cv" component={Cv} />
            <Route path="/projects" component={Projects} />
            <Route path="/skills" component={Skills} />
            <Route path="/contact" component={Contact} />
          </HashRouter>
          <ScrollButton />
        </Col>
        <Col sm={0} lg={1} className="side"></Col>
      </Row>

    </Container>
  );
};
export default App;
