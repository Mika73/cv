import data from "../data/data.json";
import {Container, Row, Col} from "react-bootstrap";
import Stars from './Stars';

const SkillDisp =(props) => {
	let array =[];
	if(props.type==="professional"){
		array = data.skills;
	}else if (props.type==="language"){
		array = data.languages;
	}
	return (
		<div>
			<Container fluid>
			{array.map((row,index) => {
				return(
				<div key={index}>
					<Row>
						<Col sm={12} md={4}>
							<h5>{row.skill}</h5>
						</Col>
						<Col sm={12} md={8}>
							<p>
								<Stars number={row.level}/>
							</p>
						</Col>
					</Row>
				</div>);
				})
			}
			</Container>
		</div>
	);
};
export default SkillDisp
