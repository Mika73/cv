import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

const HeaderButton = (props) =>{
	return (
		<div>
			<Link to={props.link}>
				<Button variant={props.color} className={props.class}>{props.text}</Button>
			</Link>
		</div>
	)
}
export default HeaderButton