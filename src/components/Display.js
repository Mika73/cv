import data from "../data/data.json";
import {Container, Row, Col} from "react-bootstrap";

const Display =(props) => {
	let array =[];
	if(props.type==="study"){
		array = data.studies.studies;
	}else if (props.type==="job"){
		array = data.jobs.jobs;
	}
	return (
		<div>
			{array.map((row,index) => {
				let sYear = row.startDate.slice( 0, 4 ) ;
				let sMonth = row.startDate.slice( 5, 7 ) ;
				let eYear = row.endDate.slice( 0, 4 ) ;
				let eMonth = row.endDate.slice( 5, 7 ) ;
				let date ="";
				if (eYear===""){
					date = sMonth + "/" + sYear + " - ";
				}else if (sYear === eYear){
					date = sMonth + "/" + sYear + " - " + eMonth + "/" + eYear;
				}else{
					date = sYear + "-" + eYear;
				}
				return(
				<div key={index} className="display mb-4">
					<Container fluid>
						<Row>
							<Col sm={12} md={2}>
							<p className="year">{date}</p>
							</Col>
							<Col sm={12} md={10} className="mb-5">
							<p className="title-wrapper"><span className="title">{row.name}</span></p>
							{row.descriptions.map((description,index) => {
								return(
									<p className="description" key={index}>- {description}</p>
								)})}
							</Col>
						</Row>
					</Container>
				</div>);
				})
			}
		</div>
	);
};
export default Display




