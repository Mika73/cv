import data from "../data/data.json";
import Display from './Display';

const Cv =() => {
	return (
		<div className='container'>
			<h2 className="my-5 border-bottom border-info">
				{data.studies.title}
			</h2>
			<Display type={"study"}/>
			<h2 className="my-5 border-bottom border-info">
				{data.jobs.title}
			</h2>
			<Display type={"job"}/>
		</div>
	);
};
export default Cv
