import data from "../data/data.json";
import Portfolio from "./Portfolio";
// import { TiFeather } from "react-icons/ti";

const Projects = () => {
  return (
    <div className="container">
      <h2 className="my-5 border-bottom border-info">{data.project.title}</h2>
      <Portfolio />
    </div>
  );
};
export default Projects;
