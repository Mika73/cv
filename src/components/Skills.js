import data from "../data/data.json";
import { Container, Row, Col } from "react-bootstrap";
import SkillDisp from "./SkillDisp";

const Skills = () => {
  let date = new Date(data.skillLastUpdate);
  let formattedDate = date.toLocaleDateString();
  return (
    <div className="container">
      <h2 className="my-5 border-bottom border-info">{data.skillsTitle}</h2>
      <p className="text-right">Dernière mis à jour le {formattedDate}</p>
      <Container fluid className="skills">
        <Row>
          <Col sm={12} lg={6}>
            <h4 className="mt-2 mb-5 p-title">Compétences</h4>
            <SkillDisp type={"professional"} />
          </Col>
          <Col sm={12} lg={6}>
            <h4 className="mt-2 mb-5 p-title">Langues</h4>
            <SkillDisp type={"language"} />
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default Skills;
